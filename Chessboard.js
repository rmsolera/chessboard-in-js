/*-------------------
Author: Ruben Munoz Solera
Application: Chessboard pattern
Date: 09/07/2020 (MM/DD/YYYY)
Description: Build a Chessboard pattern
using javascript and optional board size.
--------------------*/

let thePattern = '';
let size = parseInt(prompt('What board size do you want?: (ex: 8)') - 1);
console.log('Board size: '+ (size + 1) + ' x ' + (size + 1));

// create 7 cols by adding enter every end of this block execution
for (var col = 0; col <= size; col++)
{
	// if col position is odd, build the following pattern
	if (col %2 != 0)
	{
		for (var row = 0; row <= size; row++) 
		{
			// if row pos is even, start with #
			if (row % 2 == 0){thePattern += '#'}
			else {thePattern += ' '}
		}
	}

	//if col position is even, build the following pattern
	if (col % 2 == 0) 
	{
	// Create a string of # and spaces
		for (var row = 0; row <= size; row++) 
			{
				// if row pos is odd, start with ' '
				if (row % 2 == 0){thePattern += ' '}
				else {thePattern += '#'}
			};
	}

	// add new line
	thePattern += '\n';

};
console.log(thePattern);